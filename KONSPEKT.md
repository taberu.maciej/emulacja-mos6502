# Wstęp:
Cel, motywacja; Czym jest emulacja? Potrzebne materiały i wiedza przy implementacji emulatora.

# Rozdział 1 - Architektura komputerów.
### Jak działa komputer?
* wstęp do architektury nowoczesnego komputera, ze skupieniem na zasadzie działania zespołu procesor-pamięć.
* wytłumaczenie pojęć: rozkaz, program counter, pamięć, adres, ALU.
### Język programowania, a kod assembly?
* ukazanie świata tool-chainów, od języka wysoko-poziomowego do kodu maszynowego.
* wytłumaczenie pojęć: operator przypisania, warunek, pętla, subrutyna (funkcja), stack, heap.
### Historia układów logicznych.
* dlaczego 8 bitów?
* Uniwersalna Maszyna Turinga (1936 r.) i architektura Von Neumanna (1945 r.).
### Matematyka komputerów.
* wytłumaczenie wszystkich niezbędnych operacji na bitach.

# Rozdział 2 -  procesor MOS6502.
### Opis procesora.
* specyfikacja techniczna MOS6502,
* opis historyczny,
* cykl wykonawczy (z wytłumaczeniem trybów adresowania),
* opis rejestrów,
* opis rozkazów,
* stos,
* przerwania,
* tryb binarny/dziesiętny,
* tryby adresowania.

# Rozdział 3 - Emulacja MOS6502 w języku C/C++.
### Przygotowania wstępne.
* wstęp, opis techniczny projektu (biblioteka statyczna, struktura projektu, itp.),
* wzorowanie się na istniejących emulatorach,
* typy 8-bitowe, biblioteka stdint.h.
### Główne elementy emulacji procesora.
* pętla główna,
* implementacja trybów adresowania,
* implementacja rozkazów,
* implementacja stosu,
* implementacja przerwań.
### Rozszerzenia projektu.
* obsługa mapowania wejść/wyjść,
* idealna emulacja z wykorzystaniem wielowątkowości,
* wykorzystanie typu atomic przy operacjach na pamięci.

# Rozdział 4 - Testowanie i debugging.
### Pierwsze uruchomienie i testy w Visual Studio.
* łączenie bibliotek statycznych,
* buforowanie wiadomości z biblioteki statycznej,
* debugowanie za pomocą konsoli.
### Integracja z OpenGL.
`OpenGL, wyjątkowo, jeżeli wystarczy czasu`
* proste wyświetlanie pamięci jako piksele na ekranie.
### Integracja z Unreal Engine 4.
* łączenie bibliotek statycznych z UnrealBuildTool.
* dynamiczne tekstury wyświetlające obszar pamięci
